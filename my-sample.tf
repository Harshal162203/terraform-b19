provider "asw" {
    region = "eu-west-3"

}
resource "aws_instance" "my_instance" {
    ami = "ami-089c89a80285075f7"
    instance_type = "t2.micro"
    key_name = "harshal-nv"
    vpc_security_group_ids = "sg-0e52aa942fad731fc"
    tags {
        Name = "my-instance"
        evn = "dev"
    }
}
